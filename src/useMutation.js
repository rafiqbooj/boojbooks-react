/*
This file is being used in one of the Relay example projects (https://github.com/relayjs/relay-examples/tree/main/issue-tracker)
with 0 documentation/comments included, but the gist seems to be a helper function to provide a boilerplate using commitMutation
while ensuring only one of this mutation is running, and running an onCompleted when done (unless attached to a component that
was unmounted). It returns both [isPending, execute] so you can disable/update UI components while the mutation is running.
*/

import React from 'react';
import { useRelayEnvironment } from 'react-relay/hooks';
import { commitMutation } from 'relay-runtime';

const { useState, useRef, useCallback, useEffect } = React;

export default function useMutation(mutation) {
  const environment = useRelayEnvironment();
  const [isPending, setPending] = useState(false);
  const requestRef = useRef(null);
  const mountedRef = useRef(false);
  const execute = useCallback(
    (config = { variables: {} }) => {
      if (requestRef.current != null) {
        return;
      }
      const request = commitMutation(environment, {
        ...config,
        onCompleted: () => {
          if (!mountedRef.current) {
            return;
          }
          requestRef.current = null;
          setPending(false);
          config.onCompleted && config.onCompleted();
        },
        onError: error => {
          console.error(error);
          if (!mountedRef.current) {
            return;
          }
          requestRef.current = null;
          setPending(false);
          config.onError && config.onError(error);
        },
        mutation,
      });
      requestRef.current = request;
      setPending(true);
    },
    [mutation, environment],
  );
  useEffect(() => {
    mountedRef.current = true;
    return () => (mountedRef.current = false);
  }, []);
  return [isPending, execute];
}
