import React from 'react';
import './App.css'
import graphql from 'babel-plugin-relay/macro';
import {
  RelayEnvironmentProvider,
  loadQuery,
  usePreloadedQuery,
} from 'react-relay/hooks';
import RelayEnvironment from './RelayEnvironment';
import useMutation from './useMutation';
import { ConnectionHandler } from 'relay-runtime';

const { Suspense } = React

// define a query
const BooksQuery = graphql`
  query AppBooksQuery {
    books(first: 10) @connection(key: "App_books") {
      edges {
        node {
          id
          name
          author {
            id
            name
            thumbnail
          }
          description
          thumbnail
        }
      }
    }
  }
`

const UpdateBookMutation = graphql`
  mutation AppUpdateBookMutation($input: UpdateBookInput!) {
    updateBook(input: $input) {
      id
      name
      description
    }
  }
`

const CreateBookMutation = graphql`
  mutation AppCreateBookMutation($input: CreateBookInput!) {
    createBook(input: $input) {
      id
      name
      description
    }
  }
`

// Immediately load the query as our app starts. For a real app, we'd move this
// into our routing configuration, preloading data as we transition to new routes.
const preloadedQuery = loadQuery(RelayEnvironment, BooksQuery, {
  /* query variables */
});

// Inner component that reads the preloaded query results via `usePreloadedQuery()`.
// This works as follows:
// - If the query has completed, it returns the results of the query.
// - If the query is still pending, it "suspends" (indicates to React that the
//   component isn't ready to render yet). This will show the nearest <Suspense>
//   fallback.
// - If the query failed, it throws the failure error. For simplicity we aren't
//   handling the failure case here.
function App({ preloadedQuery }) {
  const data = usePreloadedQuery(BooksQuery, preloadedQuery);
  // console.log(data)
  return (
    <div className="App">
      <AddBook />
      <header className="App-header">
        <BookList books={data.books.edges} />
      </header>
    </div>
  );
}

function AddBook() {
  const [addBookPending, addBook] = useMutation(CreateBookMutation);

  // store ID of book list to add new books into
  const connectionID = ConnectionHandler.getConnectionID(
    'root',
    'App_books',
  );

  const newBookName = React.createRef();
  const newBookDescription = React.createRef();

  function createBook() {
    const params = {
      variables: {
        input: {
          name: newBookName.current.value,
          description: newBookDescription.current.value,
          author: {
            connect: "QXV0aG9yOjE0"
          }
        }
      },
      updater: store => {
        // get new book to add to beginning of list
        const newBook = store.getRootField('createBook')

        // get list
        const books = store.get(connectionID);

        // create new edge (with new book as the node)
        const newEdge = ConnectionHandler.createEdge(
          store,
          books,
          newBook,
          'BookEdge'
        )

        // add to beginning of list
        ConnectionHandler.insertEdgeBefore(
          books,
          newEdge
        )
      }
    }

    // clear input
    newBookName.current.value = ''
    newBookDescription.current.value = ''

    addBook(params)
  }

  return (
    <div className="add-book">
      <input 
        name="newBookName"
        type="text"
        placeholder="new book name"
        ref={newBookName}
        disabled={addBookPending}
      />
      <br />
      <textarea
        name="newBookDescription"
        placeholder="new book description"
        ref={newBookDescription}
        disabled={addBookPending}
      />
      <button
        onClick={createBook}
        disabled={addBookPending}
      >Add Book</button>
    </div>
  )
}

function BookList({ books }) {
  const bookList = books.map((edge) => 
    <Book key={edge.node.id} book={edge.node} />
  );
  return (
    <ul>
      {bookList}
    </ul>
  );
}

function Book({ book }) {
  const [bookUpdatePending, updateBook] = useMutation(UpdateBookMutation);
  const newBookName = React.createRef();
  const newBookDescription = React.createRef();

  function doBookUpdate() {
    const params = {
      variables: {
        input: {
          id: book.id
        }
      }
    }
    if (newBookName.current.value) {
      params.variables.input.name = newBookName.current.value
    }
    if (newBookDescription.current.value) {
      params.variables.input.description = newBookDescription.current.value
    }
    newBookDescription.current.value = ''
    newBookName.current.value = ''
    updateBook(params)
  }

  return (
    <div className="book">
      <li>
        <img src={book.thumbnail} height="90px" width="120px" alt="" />
        <br />
        {book.name} ({book.description}) 
        <br />
        <input 
          name="newBookName"
          type="text"
          placeholder="updated book name"
          ref={newBookName}
          disabled={bookUpdatePending}
        />
        <br />
        <textarea
          name="newBookDescription"
          placeholder="updated book description"
          ref={newBookDescription}
          disabled={bookUpdatePending}
        />
        <br />
        <button
          onClick={doBookUpdate}
          disabled={bookUpdatePending}
        >Update Book</button>
        <div>
          {bookUpdatePending && "Updating book..."}
        </div>
      </li>
    <hr />
  </div>
 )
}

// The above component needs to know how to access the Relay environment, and we
// need to specify a fallback in case it suspends:
// - <RelayEnvironmentProvider> tells child components how to talk to the current
//   Relay Environment instance
// - <Suspense> specifies a fallback in case a child suspends.
function AppRoot() {
  return (
    <RelayEnvironmentProvider environment={RelayEnvironment}>
      <Suspense fallback={'LOADING SCREEN'}>
        <App preloadedQuery={preloadedQuery} />
      </Suspense>
    </RelayEnvironmentProvider>
  );
}

export default AppRoot
