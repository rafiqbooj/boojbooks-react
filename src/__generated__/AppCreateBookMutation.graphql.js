/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type CreateBookInput = {|
  name: string,
  author: CreateAuthorBelongsTo,
  description?: ?string,
  thumbnail?: ?string,
|};
export type CreateAuthorBelongsTo = {|
  connect?: ?string,
  create?: ?CreateAuthorInput,
  update?: ?CreateAuthorInput,
|};
export type CreateAuthorInput = {|
  name: string,
  thumbnail?: ?string,
|};
export type AppCreateBookMutationVariables = {|
  input: CreateBookInput
|};
export type AppCreateBookMutationResponse = {|
  +createBook: {|
    +id: string,
    +name: string,
    +description: ?string,
  |}
|};
export type AppCreateBookMutation = {|
  variables: AppCreateBookMutationVariables,
  response: AppCreateBookMutationResponse,
|};
*/


/*
mutation AppCreateBookMutation(
  $input: CreateBookInput!
) {
  createBook(input: $input) {
    id
    name
    description
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "Book",
    "kind": "LinkedField",
    "name": "createBook",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "name",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "description",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "AppCreateBookMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "AppCreateBookMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "294442db3151b6605b71e8467552aac1",
    "id": null,
    "metadata": {},
    "name": "AppCreateBookMutation",
    "operationKind": "mutation",
    "text": "mutation AppCreateBookMutation(\n  $input: CreateBookInput!\n) {\n  createBook(input: $input) {\n    id\n    name\n    description\n  }\n}\n"
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '978a0c2904288778a8791b6d3ff12948';

module.exports = node;
