/**
 * @flow
 */

/* eslint-disable */

'use strict';

/*::
import type { ConcreteRequest } from 'relay-runtime';
export type UpdateBookInput = {|
  id: string,
  name?: ?string,
  author?: ?CreateAuthorBelongsTo,
  description?: ?string,
  thumbnail?: ?string,
|};
export type CreateAuthorBelongsTo = {|
  connect?: ?string,
  create?: ?CreateAuthorInput,
  update?: ?CreateAuthorInput,
|};
export type CreateAuthorInput = {|
  name: string,
  thumbnail?: ?string,
|};
export type AppUpdateBookMutationVariables = {|
  input: UpdateBookInput
|};
export type AppUpdateBookMutationResponse = {|
  +updateBook: ?{|
    +id: string,
    +name: string,
    +description: ?string,
  |}
|};
export type AppUpdateBookMutation = {|
  variables: AppUpdateBookMutationVariables,
  response: AppUpdateBookMutationResponse,
|};
*/


/*
mutation AppUpdateBookMutation(
  $input: UpdateBookInput!
) {
  updateBook(input: $input) {
    id
    name
    description
  }
}
*/

const node/*: ConcreteRequest*/ = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "Book",
    "kind": "LinkedField",
    "name": "updateBook",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "name",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "description",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "AppUpdateBookMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "AppUpdateBookMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "cef038aa2e46da8c1410d5a3349b30ad",
    "id": null,
    "metadata": {},
    "name": "AppUpdateBookMutation",
    "operationKind": "mutation",
    "text": "mutation AppUpdateBookMutation(\n  $input: UpdateBookInput!\n) {\n  updateBook(input: $input) {\n    id\n    name\n    description\n  }\n}\n"
  }
};
})();
// prettier-ignore
(node/*: any*/).hash = '4d519ce66fef30a3af510e84beb7ae82';

module.exports = node;
